/* *********************************
 * Global Variables
 ***********************************/
var internsArray = [];
var addButton = orly.qid("addInternButton");
var alertManager = orly.qid("alerts");

// DOM ready
orly.ready.then(function() {
	// Your table and your Add Intern divs are both showing.
	// You need to update the function "showAndHideElements"!!!
	global.showAndHideElementsById("internTableDiv", "addEditInternDiv");
	
	global.displayInterns(internsArray);
	
	// The DOM is loaded so now we can create Event Listeners
	handleButtonClick();
	
	// Welcome message
	//orly.qid("alerts").createAlert({type:"success", duration:"2000", msg:"Welcome to Intern Manager v0.001"});
	var message = "Welcome to Intern Manager v0.001";
	global.displayMessage("success", message);
	
});	

/***********************************************************************
 * Global Namespace For Functions
 **********************************************************************/
var global = {	
		
	/**
	 * Display Interns
	 */
	displayInterns : function() {
		console.log("Display Interns Called");
		this.buildAndDisplayInternTable();
		
	}, // end of displayInterns
	
	
	/**
	 * Build and display Intern Table
	 */
	buildAndDisplayInternTable : function() {
		console.log("buildAndDisplayInternTable called.");
		
		// TODO: build intern table
		let internTable = orly.qid("internTable")
		let initialRow = orly.qid("noRecordsEntry");
		
		if (internsArray.length == 0) {
			// If the internsArray is empty:
			// show an empty row that spans all columns
			// the row should read "No Data Found"
		} else {
			// If the internsArray is not empty:
			// Sort the interns (by first and/or last name)
		    // Delete the existing table
		    // Build a new table
			this.buildInternTable();
		}
		
		// USE BELOW CODE AS A REFERENCE FOR POPULATING THE INTERNS ARRAY
		
		// DELETE THE INITIAL "NO RECORDS FOUND" TABLE ROW
		if (initialRow != null && initialRow != undefined && initialRow != "") {
			internTable.removeChild(initialRow);
		}
		
		// DOM MANIPULATION - ADD "NO RECORDS FOUND" TO THE 
		// TABLE WHEN APPLICABLE. OTHERWISE LOOP THROUGH THE 
		// INTERNS ARRAY AND POPULATE THE TABLE
		
		// Create the row, the table data, and the text nodes
		let noRecordRow = document.createElement("tr");
		let noRecordData = document.createElement("td");
		let noRecordText = document.createTextNode("No Records Found");
		
		// Create a colspan attribute and set its value to 7 
		let colspan = document.createAttribute("colspan");
		colspan.value = "7";
		
		
		// Create an icon
		let editIcon = document.createElement("orly-icon");
		editIcon.name = "edit";
		editIcon.color = "wheat";
		editIcon.setAttribute("class", "ml-2");
		
		// Append the text node and image to the table data cell
		// and add the colspan attribute 
		noRecordData.appendChild(noRecordText);
		noRecordData.setAttributeNode(colspan);
		noRecordData.appendChild(editIcon);  // <== add the icon
		
		// Append the table data cell to the row. Create the id
		// attribute and set it on the row. 
		noRecordRow.appendChild(noRecordData);
		let id = document.createAttribute("id");
		id.value = "noRecordsEntry";
		noRecordRow.setAttributeNode(id);
		
		// Alternatively, we could have set the attribute
		// on the node with setAttribute(name, value);
		// noRecordRow.setAttribute("id", "noRecordsEntry"); 
		
		// Add the row to the table
		internTable.appendChild(noRecordRow);

	}, // end of buildAndDisplayInternTable
	
	
	/**
	 * Build Intern Table
	 */
	buildInternTable : function() {
		// Loop through interns
		
		
		// Build a row for each intern using DOM manipulation
		// (createElement and createTextNode)
		let intern = null;
		this.addTableRow(intern);
		
	}, // end of buildInternTable
	
	
	/**
	 * Add Table Row
	 */
	addTableRow : function(intern) {
		// Add table row for a given intern
		
		
	}, // end of addTableRow
	
	
	/**
	 * Show and Hide Elements
	 */
	showAndHideElementsById : function(showId, hideId) {
		// This function is intended to abstract the implementation
		// of showing and hiding elements on a page. We need to show
		// the "intern table" and hide the "add edit form" when the
		// page loads.
		
		// We need to hide the intern table and show the add edit
		// form when the "add" button is clicked.
		
		// Hide the element "hide" and show the element "show"
		// Use <element>.classList.add() and <element>.classList.remove()
		// here.
		orly.qid(showId).classList.remove("d-none");
		orly.qid(showId).classList.add("d-block");
		
		orly.qid(hideId).classList.remove("d-block");
		orly.qid(hideId).classList.add("d-none");
	}, // end of showAndHideElements
	
	
	/**
	 * Add Intern - Called when submitting the "Add New Intern" Form
	 */
	addIntern: function(firstName, lastName, age, startDate, graduationDate) {
		// Generate Intern Id
		let internId = this.getInternId();
		
		// Build Intern
		let intern = {};
		// intern.firstName = firstName;...
		
		// Add intern to internsArray
		
		
		// Return Intern?... depends on your implementation...
		return intern;
		
	}, // end of addIntern
	
	
	/**
	 * Get Intern Id
	 */
	getInternId: function() {
		let id = null;
		
		// Use the Math object to get a random 6-digit
		// number that starts with a 4. The downside to 
		// this approach is that you may accidentally return
		// the same id twice... but highly unlikely in the lab
		// :)
		
		// Or you can place "var id = 400000;" at the top
		// of the script and return id++ so that the current
		// id is returned and then incremented.
		
		return id;
	}, // end of getInternId
	
	
	/**
	 * Edit Intern - Called after editing an intern
	 */
	editIntern: function(id, firstName, lastName, age, startDate, graduationDate) {
		// Update the intern in the internsArray
		// Loop through the intern and find the intern with the id that
		// was passed into the function. Once found, update the intern
		// details. Set the modified intern back on the array.
		
		
		// Refresh the table to show the list of interns.
		
		
	}, // end of editIntern
	
	
	/**
	 * Delete Intern
	 */
	deleteIntern: function() {
		// Delete the intern from the internsArray
		// Update the table
		
	}, // end of deleteIntern
	
	
	/**
	 * Display Message
	 * This function abstracts the implementation to show
	 * success and error messages.
	 * 
	 * Valid types = primary, secondary, success, danger, 
	 *               warning, info, light, dark
	 * 
	 */
	displayMessage: function(type, message) {
		var thisType = type.toLowerCase();
		alertManager.createAlert({type:type, duration:"2000", msg:message});
	}, // end of displayMessage
	
	
	/**
	 * Find Intern By Key
	 */
	findInternById: function(searchKey, searchText) {
		var result = [];
		
		// Loop through interns
		// Find the intern using the key (id, firstName, lastName...)
		// and value (searchText)
		// If found, show the table with one or more entries
		// If not found, show "no results"
		
		// For example, if searchKey = "firstName" and searchText = "Bob"
		// Then interns with "Bob" in the first name field should be returned.
		
		
		return result;
	} // end of findInternById
	
}; // end of global

// Global functions
function handleButtonClick() {
	orly.on(addButton, "click", (e)=> {
		console.log("click!");
		setTimeout(function(){ 
			addButton.stopSpinning();
		}, 2000);
	});
}
