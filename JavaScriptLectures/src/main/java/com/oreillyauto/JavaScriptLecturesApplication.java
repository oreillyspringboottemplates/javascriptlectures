package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaScriptLecturesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaScriptLecturesApplication.class, args);
	}

}
